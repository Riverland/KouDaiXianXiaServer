#coding:utf8
'''
Created on 2013-9-22
怪物
@author: jt
'''
from app.scense import tables


class Monster(object):
    ''' 怪物 '''

    def __init__(self,mosterid,fpetid=0):
        ''' 初始化 '''
        from app.scense.core.pet.skill import Skill
        self.monsterid=mosterid    #怪物表 moster 主键id
        self.fpetid=fpetid         #怪物战斗id
        info=tables.monster_table[mosterid]  #怪物表
        self.ispet=False #是否是宠物
        self.name=info['pname']    #怪物名称
        self.resourceid=info['resourceid'] #怪物资源id
        self.color=info['color']   #怪物名称颜色  1=白色 2=蓝色 3=紫色 4=橙色
        self.btype=info['btype']   #战斗类型 1=近战 2=远程 3=加血
        self.maxhp=info['hp']           #血量、生命值 、血量值
        self.hp=self.maxhp              #当前血量
        self.att=info['att']            #攻击值
        self.defe=info['def']           #防御值
        self.dex_base=info['dex']       #命中
        self.agl_base=info['agl']       #闪避
        self.par_base=info['par']       #格挡
        self.cri_base=info['cri']       #暴击
        self.crp_base=info['crp']       #暴击威力
        self.tou_base=info['tou']       #韧性
        self.ske_base=info['ske']       #偏斜
        self.spd=info['spd']       #攻击速度
        self.mov=info['mov']       #移动速度
        self.rng=info['rng']       #攻击距离
        self.coatt=info['coatt']   #目标改变   1会改变目标（petid越小越会被选中）   2平均随机目标   3锁定一个目标往死里打
        self.dou=info['dou']       #破体
        self.skillList=[]
        for sid in info['skilllist']:
            self.skillList.append(Skill(sid))
        self.updateAttribute()  
        
    def updateAttribute(self):
        '''更新角色属性值'''
        modulus=tables.modulus_table #系数
        self.dex=self.dex_base  #命中值
        self.agl=self.agl_base  #闪避值
        self.par=self.par_base  #格挡值
        self.cri=self.cri_base  #暴击值
        self.crp=self.crp_base  #暴击威力值
        self.tou=self.tou_base  #韧性值
        self.ske=self.ske_base  #偏斜值
        
        hpc=modulus['hp']   #生命系数、标准生命
        attc=modulus['att'] #攻击系数、标准攻击
        
        #伤害值
        self.harm=self.att*(self.cri*modulus['cri']/attc+1)*(self.crp*modulus['crp']/attc+1)*(self.dex*modulus['dex']/attc+1)
        #伤害率
        self.harml=1/(1+2.5*self.defe/hpc)
        #生存值
        survive=self.maxhp*(0.5*self.defe*modulus['def']/hpc+1)*(self.agl*modulus['agl']/hpc+1)*(self.tou*modulus['tou']/hpc+1)*(self.par*modulus['par']/hpc+1)*(self.ske*modulus['ske']/hpc+1)
        #战斗力
        self.fn=int(survive*self.harm/10000)
        
    def getAllPetInfo(self,pinfo):
        '''获取怪物信息
        @param pinfo: obj proto类
        '''
        pinfo.petid=self.monsterid #怪物模板id
        pinfo.fpetid=self.fpetid   #怪物战斗id
        pinfo.name=self.name
        pinfo.color=self.color
        pinfo.btype=self.btype
        pinfo.hp=self.maxhp
        pinfo.att=self.att
        pinfo.defe=self.defe
        pinfo.dex=self.dex_base
        pinfo.agl=self.agl_base
        pinfo.par=self.par_base
        pinfo.cri=self.cri_base
        pinfo.crp=self.crp_base
        pinfo.tou=self.tou_base
        pinfo.ske=self.ske_base
        pinfo.spd=self.spd
        pinfo.mov=self.mov
        pinfo.rng=self.rng
        pinfo.coatt=self.coatt
        pinfo.dou=self.dou
        pinfo.resourceid=self.resourceid
        if len(self.skillList)<1:
            pinfo.skill.extend([])
        else:
            for item in self.skillList:
                skill=pinfo.skill.add()
                item.getSkillInfo(skill)
        
    def addHP(self,count):
        '''加血'''
        self.hp+=count
        if self.hp>self.maxhp:
            self.hp=self.maxhp
            
    def lowerHp(self,count):
        '''减血
        @param count: int 较少多少血量
        @return: int 剩余血量
        '''
        self.hp-=count
        if self.hp<0:
            self.hp=0
        return self.hp
        
        
        
        
        