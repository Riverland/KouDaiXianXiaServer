#coding:utf8
'''
Created on 2013-9-22
宠物
@author: jt
'''
from app.mem.mPlayer import playerPet_m
from app.js import Obj
import random
from app.scense.core.language.language import Lg
#from app.scense import tables
from app.scense.tables import configs_table,intensify_table,growing_table,pet_table,modulus_table,pet_exp

class Pet(object):
    ''' 角色宠物 '''


    def __init__(self,ppid):
        ''' 初始化 '''
        from app.scense.core.pet.skill import Skill
#        from app.scense import tables
        pinfos=playerPet_m.getObj(ppid)
        self.ispet=True #是否是宠物
        pinfo=pinfos.get('data')
        self.ppid=ppid             #角色拥有宠物id
        self.level=pinfo.get('level',1) #角色的等级
        self.exp=pinfo.get('exp',0)#宠物当前经验值
        self.petid=pinfo.get('petid') #宠物表 pet 主键id
        self.pid=pinfo.get('pid')  #宠物拥有者的id
        self.silver=pinfo.get('silver')     #解锁需要的银币数量
        self.flg=bool(pinfo.get('unlocks',0))    #是否已经解锁
        info=pet_table#.get(self.petid)     #宠物表
        info = info.get(self.petid)
        self.resourceid=info.get('resourceid',0) #宠物资源id
        self.headid=info.get('headid') #宠物头像id 小图标id
        self.name=info['pname']    #宠物名称
        self.gid=info['gid']       #成长值 growing表外键id
        newgid=pinfo.get("newgid",0)#后期成长值
        if newgid>0:
            self.gid=newgid
        self.qhhplv=pinfo.get('qhhplv',0)#宠物强化Hp的等级
        self.qhattlv=pinfo.get('qhattlv',0)#宠物强化攻击力的等级
        growinginfo=growing_table.get(self.gid)#成长值
        self.btype=info['btype']   #战斗类型 1=近战 2=远程 3=加血
        self.hp_base=info['hp']         #基础血量
        self.att_base=info['att']       #基础攻击
        self.defe_base=info['def']      #基础防御
        self.dex_base=info['dex']       #基础命中
        self.agl_base=info['agl']       #基础闪避
        self.par_base=info['par']       #基础格挡
        self.cri_base=info['cri']       #基础暴击
        self.crp_base=info['crp']       #基础暴击威力
        self.tou_base=info['tou']       #基础韧性
        self.ske_base=info['ske']       #基础偏斜
        self.spd=info['spd']       #攻击速度
        self.mov=info['mov']       #移动速度
        self.rng=info['rng']       #攻击距离
        self.coatt=info['coatt']   #目标改变   1会改变目标（petid越小越会被选中）   2平均随机目标   3锁定一个目标往死里打
        self.dou=info['dou']       #破体
        
        self.desc=growinginfo['des']                 #成长描述
        self.color=growinginfo['color']    #宠物名称颜色  1=白色 2=蓝色 3=紫色 4=橙色
        self.qhlimit=growinginfo['qhlimit'] #可强化等级上限
        self.upmoney=growinginfo['upmoney'] #渡劫需要的银币数量
        self.skillList=[]
        for sid in info['skilllist']:
            self.skillList.append(Skill(sid))
        self.updateAttribute()
        
        
    def addExp(self,exp):
        '''给宠物增加经验
        @param exp: int 要增加的经验
        '''
#        from app.scense.tables import pet_exp
        if self.level >= 10:#满级
            return
        self.exp += exp
        pinfo = playerPet_m.getObj(self.ppid)
        data = pinfo.get("data")
        state = 0
        while 1:
            needExp = pet_exp[self.level]
            if self.exp < needExp:
                break
            self.exp -= needExp
            self.level+=1
            state = 1
            if self.level == 10:
                self.exp = 0
                break
        data['exp'] = self.exp
        data['level'] = self.level
        pinfo.update_multi(data)
        if state:
            self.updateAttribute()
        
    def updateAttribute(self):
        '''更新角色属性值'''
#        from app.scense import tables
        growinginfo=growing_table.get(self.gid)#成长值
        modulus=modulus_table #系数
        self.maxhp=self.hp_base+(growinginfo['hp']*self.level)#血量、生命值 、血量值
        self.hp=self.maxhp #当前血量
        qhHp = self.countAllAtt(1)#宠物强化所加的生命值
        self.maxhp += qhHp
        
        self.att=self.att_base+(growinginfo['att']*self.level)  #攻击力值
        self.defe=self.defe_base+(growinginfo['def']*self.level)#防御力值
        self.dex=self.dex_base+(growinginfo['dex']*self.level)  #命中值
        self.agl=self.agl_base+(growinginfo['agl']*self.level)  #闪避值
        self.par=self.par_base+(growinginfo['par']*self.level)  #格挡值
        self.cri=self.cri_base+(growinginfo['cri']*self.level)  #暴击值 暴击率值
        self.crp=self.crp_base+(growinginfo['crp']*self.level)  #暴击威力值
        self.tou=self.tou_base+(growinginfo['tou']*self.level)  #韧性值
        self.ske=self.ske_base+(growinginfo['ske']*self.level)  #偏斜值
        
        self.gdl=2.0*self.par*modulus['par']/modulus['hp']      #格挡率  0.04

        qhAtt = self.countAllAtt(2)#宠物强化所加的攻击
        self.att += qhAtt
        
        hpc=modulus['hp']   #生命系数、标准生命
        attc=modulus['att'] #攻击系数、标准攻击
        
        #伤害值  278.1575418   457.65461
        self.harm=self.att*(self.cri*modulus['cri']/attc+1)*(self.crp*modulus['crp']/attc+1)*(self.dex*modulus['dex']/attc+1)
        #伤害率 0.9168457     0.863548
        self.harml=1/(1+2.5*self.defe/hpc)
        #生存值 16109.8       22174.75333333
#        survive=self.maxhp*(0.5*self.defe*modulus['def']/hpc+1)*(self.agl*modulus['agl']/hpc+1)*(self.tou*modulus['tou']/hpc+1)*(self.par*modulus['par']/hpc+1)*(self.ske*modulus['ske']/hpc+1)
        survive=self.maxhp+(0.5*self.defe*modulus['def']/hpc+1)+(self.agl*modulus['agl']/hpc+1)+(self.tou*modulus['tou']/hpc+1)+(self.par*modulus['par']/hpc+1)+(self.ske*modulus['ske']/hpc+1)
        #战斗力448            1014
        self.fn=int(survive*self.harm/10000)
        
    def getAllPetInfo(self,pinfo):
        '''获取宠物信息
        @param pinfo: obj proto类
        '''
        pinfo.ppid=self.ppid #角色宠物id
        pinfo.level=self.level
        pinfo.fpetid=self.petid
        pinfo.name=self.name
        pinfo.color=self.color
        pinfo.gid=self.gid
        pinfo.btype=self.btype
        pinfo.hp_base=float(self.hp_base)
        
        pinfo.att_base=float(self.att_base)
        pinfo.defe_base=float(self.defe_base)
        pinfo.dex_base=float(self.dex_base)
        pinfo.agl_base=float(self.agl_base)
        pinfo.par_base=float(self.par_base)
        pinfo.cri_base=float(self.cri_base)
        pinfo.crp_base=float(self.crp_base)
        pinfo.tou_base=float(self.tou_base)
        pinfo.ske_base=float(self.ske_base)
        pinfo.spd=float(self.spd)
        pinfo.mov=float(self.mov)
        pinfo.rng=float(self.rng)
        pinfo.coatt=float(self.coatt)
        pinfo.dou=float(self.dou)
        pinfo.skill=[]
        if len(self.skillList)>0:
            for item in self.skillList:
                #skill=Obj()
                #item.getSkillInfo(skill)
                pinfo.skill.append(item.id)
                
        pinfo.headid=self.headid          #宠物移动中显示的小头像id
        pinfo.resourceid=self.resourceid  #宠物的资源id
        
    def getShowPetInfo(self,pinfo):
        '''获取宠物展示列表'''
        pinfo.name=self.name  #宠物名称
        pinfo.color=self.color #宠物名称颜色
        pinfo.resource=self.resourceid  #宠物资源id
        pinfo.level=self.level  #宠物等级
        pinfo.hp=self.hp
        pinfo.att=self.att
        pinfo.deff=self.defe
        pinfo.desc=self.desc
        pinfo.ppid=self.ppid #角色宠物id
        pinfo.silver=self.silver
        pinfo.flg=self.flg
        pinfo.exp=self.exp
        
    def addHP(self,count):
        '''加血'''
        self.hp+=count
        if self.hp>self.maxhp:
            self.hp=self.maxhp
            
    def lowerHp(self,count):
        '''减血
        @param count: int 减少的血量
        @return: int 当前血量值
        '''
        self.hp-=count
        if self.hp<0:
            self.hp=0
        return self.hp
        
    def backHP(self):
        '''回满血'''
        self.hp=self.maxhp
        
    def countAllAtt(self,typeId):
        '''计算宠物强化增加的总属性
        @param typeId: int 强化类型  1强化生命  2强化攻击力
        '''
#        from app.scense.tables import configs_table
        if typeId == 1:
            formula = configs_table.get(1000)
            qhlevel = self.qhhplv
        else:
            formula = configs_table.get(1001)
            qhlevel = self.qhattlv
        gid = self.gid
        hp = eval(formula)
        return int(hp)
    
    def countAddAtt(self,typeId):
        '''计算强化后增加的属性
        @param typeId: int 强化类型  1强化生命  2强化攻击力
        '''
#        from app.scense.tables import configs_table
        if typeId == 1:
            formula = configs_table.get(1000)
            qhlevel = self.qhhplv
        else:
            formula = configs_table.get(1001)
            qhlevel = self.qhattlv
        gid = self.gid
        nowAtt = eval(formula)
        qhlevel -= 1
        beforeAtt = eval(formula)
        if qhlevel < 1:
            beforeAtt = 0
        result = nowAtt - beforeAtt
        return result
    
    def countDJAdd(self,typeId):
        '''计算渡劫后增加的属性
        @param typeId: int 要计算的属性类型  1强化生命  2强化攻击力
        '''
#        from app.scense.tables import configs_table
        if typeId == 1:
            formula = configs_table.get(1000)
            qhlevel = self.qhhplv
        else:
            formula = configs_table.get(1001)
            qhlevel = self.qhattlv
        gid = self.gid
        nowAtt = eval(formula)
        gid -= 1
        beforeAtt = eval(formula)
        result = nowAtt - beforeAtt
        return result
    
    def intensifyPet(self,typeId,money,luck,response):
        '''强化宠物
        @param typeId: int 强化类型   1强化生命  2强化攻击力 3渡劫（升级品质）
        @param money: int 角色拥有的银币
        @param luck: int 角色当前幸运值
        '''
        if not self.flg:
            response.result = False
            response.message = Lg().g(1002)
            return None,None,None
        #playerInfoObj = player_m.getObj(self._owner.id)
        if typeId == 1 or typeId == 2:#强化属性
#            from app.scense.tables import intensify_table,growing_table
            if typeId == 1:
                typeStr = 'qhhplv'
            else:
                typeStr = 'qhattlv'
            petInfoObj = playerPet_m.getObj(self.ppid)
            petInfo = petInfoObj.get("data")
            growingInfo = growing_table[self.gid]
            qhlimit = growingInfo['qhlimit']/2#宠物强化上限
            if petInfo[typeStr] == qhlimit:
                response.result = False
                response.message = Lg().g(1003)
                return None,None,None
            needMoney = intensify_table.get(petInfo[typeStr]+1)['money']#强化需要金钱
            if  money < needMoney:
                response.result = False
                response.message = Lg().g(1004)
                return None,None,None
            probability = intensify_table.get(petInfo[typeStr]+1)['probability']#强化成功几率
            #playerInfo = playerInfoObj.get("data")
            response.data = []
            info = Obj()
            info.intensifyResult = True
            resultStr = "成功"
            info.addhp = 0
            info.hpmoney = 1
            info.addatt = 0
            info.attmoney = 1
            info.djmoney = 1
            info.newfighting = 0
            info.newcolor = 0
            info.newlevellimit = 1
            if random.randint(1,100) > probability:
                info.intensifyResult = False
                resultStr = "失败"
                luck += 10#幸运值+10
                #playerInfo['luck'] = luck
            else:
                petInfo[typeStr] += 1#强化等级+1
                if typeId == 1:
                    self.qhhplv += 1
                    info.addhp = self.countAddAtt(typeId)
                    info.hpmoney = 0
                    if self.qhhplv != qhlimit:
                        info.hpmoney = intensify_table.get(self.qhhplv+1)['money']
                else:
                    self.qhattlv += 1
                    info.addatt = self.countAddAtt(typeId)
                    info.attmoney = 0
                    if self.qhattlv != qhlimit:
                        info.attmoney = intensify_table.get(self.qhattlv+1)['money']
                #info.newfighting = player.countFn()
                petInfoObj.update(typeStr, petInfo[typeStr])
                petInfoObj.syncDB()
                luck = 0#幸运值清零
                #playerInfo['luck'] = 0
            money -= needMoney
            #playerInfo['silvermoney'] -= needMoney
            #playerInfoObj.update_multi(playerInfo)
            info.money = needMoney
            info.luck = luck
            response.result = True
            response.message = Lg().g(1005) % resultStr
            response.data.append(info)
            return money,luck,info.intensifyResult
        if typeId ==3:#渡劫
            if self.gid >= len(growing_table):#顶级宠物
                response.result = False
                response.message = Lg().g(1007)
                return None,None,None
#            from app.scense.tables import intensify_table,growing_table
            growingInfo = growing_table[self.gid]
            qhlimit = growingInfo['qhlimit']/2
            if self.qhhplv != qhlimit or self.qhattlv != qhlimit:
                response.result = False
                response.message = Lg().g(1006)
                return None,None,None
            needMoney = growingInfo['upmoney']
            if money < needMoney:
                response.result = False
                response.message = Lg().g(1004)
                return None,None,None
            response.result = True
            response.message = Lg().g(1008)
            response.data = []
            info = Obj()
            money -= needMoney
            #playerInfoObj.update("silvermoney", player.silvermoney)
            petInfoObj = playerPet_m.getObj(self.ppid)
            petInfoObj.update("newgid", self.gid + 1)
            petInfoObj.syncDB()#
            info.addhp = self.countDJAdd(1)
            info.hpmoney = intensify_table.get(self.qhhplv+1)['money']
            info.addatt = self.countDJAdd(2)
            info.attmoney = intensify_table.get(self.qhattlv+1)['money']
            newGrowingInfo = growing_table[self.gid+1]
            info.djmoney = newGrowingInfo['upmoney']
            #info.newfighting = player.countFn()
            info.newfighting = 0
            info.newcolor = newGrowingInfo['color']
            info.newlevellimit = newGrowingInfo['qhlimit']
            info.luck = luck
            info.money = needMoney
            info.intensifyResult = True
            response.data.append(info)
            return money,luck,info.intensifyResult
        