#coding:utf8
'''
Created on 2013-9-26

@author: jt
'''
from firefly.server.globalobject import remoteserviceHandle
from app.scense.core.pet.pet import Pet
from app import js
from app.scense.applyInterface import petInfo_app

@remoteserviceHandle('gate')
def getPlayerPets_1103(did,data):
    '''获取团队界面信息
    @param did: int 登陆动态id
    @param data: str 客户端发送过来的信息
    '''
    info=js.load(data)
    pid=info['pid']
    rsp=js.Obj()
    petInfo_app.getAllPetInfo(pid, rsp)
    return js.objstr(rsp)
    
    
@remoteserviceHandle('gate')
def getPlayerPets_1104(did,data):
    '''获取出战宠物详细信息
    @param did: int 登陆动态id
    @param data: str 客户端发送过来的信息
    '''
    info=js.load(data)
    pid=info['pid'] #角色id
    rsp=js.Obj()
    petInfo_app.getPlayerPets(pid, rsp)
    return js.objstr(rsp)
    
    
@remoteserviceHandle('gate')
def movePet_1105(did,data):
    '''移入宠物到备战区
    @param did: int 登陆动态id
    @param data: str 客户端发送过来的信息
    '''
    info=js.load(data)
    pid=info['pid'] #角色id
    moveid=info['moveid']
    targetid=info['targetid']
    rsp=js.Obj()
    petInfo_app.movePet(pid, moveid, targetid, rsp)
    return js.objstr(rsp)



@remoteserviceHandle('gate')
def moveHalt_1106(did,data):
    '''从备战区放到休息区
    @param did: int 登陆动态id
    @param data: str 客户端发送过来的信息
    '''
    info=js.load(data)
    pid=info['pid'] #角色id
    moveid=info['moveid'] #角色宠物id
    targetid=info['targetid']#位置id
    rsp=js.Obj()
    petInfo_app.moveHalt(pid, moveid, targetid, rsp)
    return js.objstr(rsp)


@remoteserviceHandle('gate')
def unlockPet_1107(did,data):
    '''花银子解锁宠物
    @param did: int 登陆动态id
    @param data: str 客户端发送过来的信息
    '''
    info=js.load(data)
    pid=info['pid'] #角色id
    ppid=info['ppid'] #角色宠物id
    rsp=js.Obj()
    petInfo_app.unlockPet(pid,ppid,rsp)
    return js.objstr(rsp)




    
    
    
    
    